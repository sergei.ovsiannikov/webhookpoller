package so.sandbox;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class Poller {

    private String baseUrl = "http://localhost:8180";
    private String listenerUrl = "https://api.pipedream.com/v1/sources/dc_gzul3BL";
    private String listenerKey = "baf1bbef1b27d73f79d9002948bc8760";

    private boolean active;
    private final Set<String> processedIds;
    private final HttpClient client;
    private final ObjectMapper mapper;

    public static void main(String[] args) throws IOException, InterruptedException {
        final Poller poller = new Poller();

        poller.loadProperties("config.properties");
        poller.clearData();
        while(poller.active) {
            poller.processMessages();
        }
    }

    private void loadProperties(final String path) throws IOException {
        try (final InputStream input = Poller.class.getClassLoader().getResourceAsStream(path)) {
            final Properties properties = new Properties();
            properties.load(input);
            baseUrl = properties.getProperty("baseUrl", baseUrl);
            listenerUrl = properties.getProperty("listenerUrl", listenerUrl);
            listenerKey = properties.getProperty("listenerKey", listenerKey);
        }
    }

    public Poller() {
        active = true;
        client = HttpClient.newBuilder()
                .connectTimeout(Duration.ofSeconds(20))
                .build();
        mapper = new ObjectMapper();
        processedIds = new HashSet<>();
    }

    private void clearData() throws IOException, InterruptedException {
        final HttpRequest request = HttpRequest.newBuilder()
                .DELETE()
                .uri(URI.create(listenerUrl + "/events"))
                .header("Authorization", "Bearer " + listenerKey)
                .build();
        client.send(request, HttpResponse.BodyHandlers.ofString());
    }

    private void processMessages() throws IOException, InterruptedException {

        final HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(listenerUrl + "/event_summaries"))
                .header("Authorization", "Bearer " + listenerKey)
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        if (response.statusCode() == HttpURLConnection.HTTP_OK) {
            final PipedreamResponse pipedreamResponse = mapper.readValue(response.body(), PipedreamResponse.class);
            processResponse(pipedreamResponse);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void processResponse(final PipedreamResponse pipedreamResponse) {
        System.out.print(".");
        for (WebhookData webhookData : pipedreamResponse.getData()) {
            if(processedIds.add(webhookData.getId())) {
                final WebhookEvent event = webhookData.getEvent();
                if (event == null) {
                    System.out.println("!!!! " + webhookData);
                    continue;
                }
                System.out.println(event);

                final HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create(baseUrl + event.getPath()))
                        .POST(HttpRequest.BodyPublishers.ofString(event.getBodyRaw()))
                        .build();
                client.sendAsync(request, HttpResponse.BodyHandlers.ofString());
            }
        }
        // active = false;
    }
}
