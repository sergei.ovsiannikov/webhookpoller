package so.sandbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.ToString;

import java.util.Map;

@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class WebhookEvent {
    private String method;
    private String path;
    private Map<String,String> headers;
    private String bodyRaw;
}
