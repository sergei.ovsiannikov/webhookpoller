package so.sandbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.ToString;

import java.util.Set;

@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class PipedreamResponse {
    private Set<WebhookData> data;
}
